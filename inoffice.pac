/*
* list for china GFW
**/

function FindProxyForURL(url, host)
{
    url = url.toLowerCase();
    host = host.toLowerCase();

// hk 
if (isInNet(dnsResolve(host), "10.120.48.0", "255.255.255.0")) {return "SOCKS5 proxy2.vxlan.net:9999";}

// hk ntt
if (isInNet(dnsResolve(host), "10.120.244.0", "255.255.255.0")) {return "SOCKS5 proxy2.vxlan.net:9998";}

// us la
if (isInNet(dnsResolve(host), "10.120.8.0", "255.255.255.0")) {return "SOCKS5 proxy2.vxlan.net:9997";}
if (isInNet(dnsResolve(host), "10.120.14.0", "255.255.255.0")) {return "SOCKS5 proxy2.vxlan.net:9997";}

return "DIRECT";

}