#!/bin/bash

# change hostname with x-x-x-x
IP=`ifconfig | grep 'inet addr:10' | awk '{print $2}' | awk -F: '{print $2}'`
HOSTNAME="`echo ${IP} | sed s/\\\./-/g`"
echo $HOSTNAME
echo "NETWORKING=yes
HOSTNAME=${HOSTNAME}" > /etc/sysconfig/network
hostname ${HOSTNAME}
echo "Configre HOSTNAME [DONE]"

history -c

