#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
key="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw0g2qYjp2RK3tZ3DRAKJjw9QFRdUcGBcpETZxLznPQMHwxmvsb4/cFoeJTDpfgueMNn8ja5QE7cpxk1zq9MXcLj8eeGkfSjhPr3TeD2nk8jtsy2mdyY+NmsQaTA6RvBrp4ZI7ac+HiwP1HSAJhZkUKJem6Huw3UBeWdYvoesHMyMBFI1Cy20O8ObnPfl6HVAjEao5xxyGz2KKA716VWgjuixTDIFx3VtA1/6NSl75xCRzzLr+xBYQIfGOH4w812svA1IolbbftZYDZLi1Lb/8LuM/Ms47PHRBK0fqO1g2uUuokz/6duI6cAPO3UsqN04+acEvTO29OfYR6TutSHDWQ== "
cd /root/.ssh/  && echo "Enter Dir OK" || echo "Enter Dir Error"
 touch authorized_keys  && echo "touch key OK" || echo "touch key error"
 cp authorized_keys authorized_keys.bak${timeNow}  && echo "backup ok" || echo "backup error"
 chattr -i -a authorized_keys  && echo "clear att ok" || echo "clear att error"
 sed -i '/root@10-154-156-115/d' authorized_keys  && echo "replace ok" || echo "replace error"
 echo $key >> authorized_keys && echo "Put key OK" && echo "insert key ok" || echo "insert key error"


Host="sshd:10.154.156.115"
cd /etc && cp hosts.allow hosts.allow.bak${timeNow} && touch hosts.allow && chattr -i -a hosts.allow && sed -i '/sshd:10.154.156.115/d' hosts.allow && echo $Host >> hosts.allow

history -c
