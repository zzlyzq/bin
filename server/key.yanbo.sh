#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`


ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAlpLOjNZkZyES7wCXeppn20/ayVlS2jOKJCBTlIzfXepCj++wVfIMM31NuXvWzWmtr97SC0G7Vot73bRg8NN/oeXWJJwat7YrZjaGt4GGF4dT+oM5ahly8ptlsc39UKwgdr0L7AyuCopHph79N9rTQwWt06H8u9M8S1l1hdusqeF/74Hc6PSeZdQzCIPdJ+8u76qrsiR7ZJu003dIyfvgIhY4wHi/HMNSwsc8jON0XIjaY5TtNu9TbzQYANaumQrwl8MRmPrCTJFzsoXkeoH8eow4o3pfLHElHiSdf8fosHKOxT78Y34zjw+dicz7Dx15IboC8uyDNQGwuTM1xWpMZw== root@vm-10-154-29-211"


ciKey2="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArdqdiusLJJSdkOzWRN3/eMULX5sDXrJqaF9R7393/ho584GuEOWNfrl2FAP9MyQUyPV4R2WZ2sNzkSmXsIQycW2aUcIiwxtRo1lyaGyZRmj0nWb9McKNZG8jrFpEE8VM2SWrEqAPf/jGFhepQaobuaKjqyjhd6QARMnPgn2uv7Kq+aOuBy9ORlJ/yhNf86Tf30Hf1fOYhED3O2gtu+3ccQCv1zueIB44VLCMLk44hbrcU1net11z8C73e/b00Rg/mMbNty1gbcHxPamf89Edd1IXdw+dHQGscUY7SCYGN8hXc3Bi37Njy3t6u47be02GMTRYwnZTNR/Gr/zbfdheUQ== root@10-180-92-207"

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && echo $ciKey >> authorized_keys2 && echo $ciKey2 >> authorized_keys2

# 部署key
    targetFile="services_hosts_allow"
    targetLine="10.154.29.211"
    targetLine2="10.180.92.207"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile &&  echo $targetLine2 >> $targetFile

history -c

