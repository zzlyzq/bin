#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
key="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuqR6QZYmVLooHbGS+Rdq6pnMD2NVgtO3GFxbbxpD/w1DcZ75cSA7r06CLh+7/83slQVCL8145bs/dgr3Fi2W7UDW7kYVl9NsJs85RTkDqFm8Nj4VlL03pWMrieGQe8wbzycT1lt+oStIGY/IZPg8A2t2uh4724xpna/gESEcdTtJH5sIqAyD1UEtUm0N4ppZaD6rK1tb4ywCB4Q19Mm/O95MQNm8bzIhLXiJJi5/PzI+rqrcfjracOSh1ioUXc2TVP17F/yGNk854LHbjvnwXjnGLxqSif+16ak62nlUJSNF5enuKPP5vGmyXNuo39xk6pwarO0fjn0kbck+3wZk7Q== root@hadoopNN1.com"
cd /root/.ssh/  && echo "Enter Dir OK" || echo "Enter Dir Error"
 touch authorized_keys  && echo "touch key OK" || echo "touch key error"
 cp authorized_keys authorized_keys.bak${timeNow}  && echo "backup ok" || echo "backup error"
 chattr -i -a authorized_keys  && echo "clear att ok" || echo "clear att error"
 sed -i '/root@10-100-91-36/d' authorized_keys  && echo "replace ok" || echo "replace error"
 echo $key >> authorized_keys && echo "Put key OK" && echo "insert key ok" || echo "insert key error"


Host="sshd:10.100.91.36"
cd /etc && cp hosts.allow hosts.allow.bak${timeNow} && touch hosts.allow && chattr -i -a hosts.allow && sed -i '/sshd:10.1100.91.36/d' hosts.allow && echo $Host >> hosts.allow

history -c
