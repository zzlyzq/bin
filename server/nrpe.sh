#!/bin/bash

# 如果是老版本,主要是乐视网接收机，那么就更新下配置就可以了
[ -e "/usr/local/nagios/etc/nrpe.cfg" ] && cd /usr/local/nagios/etc/ && rsync -avzP bigdata.vxlan.net::d/nagios/nrpe.cfg . && service xinetd restart && exit 0

# 如果是新版本已经安装，那么也是只需要更新下配置就可以了
[ -e "/etc/nagios/nrpe.cfg" ] && cd /etc/nagios/nrpe.cfg && rsync -avzP bigdata.vxlan.net::d/nagios/nrpe.cfg . && service nrpe restart && exit 0

# 安装的办法
OSNUM=`cat /etc/redhat-release | awk '{print $3}' | awk -F. '{print $1}'`
[ $OSNUM -eq 5 ] && filename="one.repo.5" || filename="one.repo"
echo $filename
cd /etc/yum.repos.d/ && mkdir bak ; mv -f * bak/ ; rsync -avzP bigdata.vxlan.net::d/repo/$filename one.repo
sed -i 's:enabled=1:enabled=0:g' /etc/yum/pluginconf.d/fastestmirror.conf
export http_proxy="http://10.180.86.30:3128/"
yum clean all && yum makecache
yum --enablerepo=epel -y install nrpe nagios-plugins

mkdir -p /usr/local/nagios/libexec/
cd /usr/local/nagios/libexec/ && rsync -avzP bigdata.vxlan.net::d/nagios/plugins/* .
chown -R nrpe:nrpe /usr/local/nagios/libexec/

cd /etc/nagios && rsync -avzP bigdata.vxlan.net::d/nagios/nrpe.cfg .
service nrpe restart
chkconfig nrpe on
