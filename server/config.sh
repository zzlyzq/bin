#!/bin/bash
# For Hadoop Machine Init

IP=`ifconfig | grep 'inet addr:10' | awk '{print $2}' | awk -F: '{print $2}'`

HOSTNAME=`echo ${IP} | sed s/\\\./-/g`
echo "NETWORKING=yes
HOSTNAME=${HOSTNAME}" > /etc/sysconfig/network
hostname ${HOSTNAME}
echo "Configre HOSTNAME [DONE]"

echo "Configure HOSTS FILE."
echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
${IP}	${HOSTNAME}" > /etc/hosts
echo "Configure HOSTS FILE. [DONE]"

echo "Configure Password"
echo "HdpSlave@%" | passwd root --stdin
#echo "C4efbo/70X31@dk" | passwd root --stdin

echo "Configure ulimit"
echo "*   soft    nofile  600000
*   hard    nofile  600000
*   soft    nproc   65536
*   hard    nproc   65536" > /etc/security/limits.d/letv.conf

history -c
