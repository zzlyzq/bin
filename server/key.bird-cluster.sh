#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuUuwZ/wLv9G9axaMKVi98HDlN0Srhf7kGwIGIOyodvAr3tgFpfh9B21XXvruOcmJZkI2OBzVSGeEhnHcldUhH5CrPQ8TjKKpSBisPbwTuLRteEqCIvhNH+VvBsrAL92EkU+PpMzEH7bb/JaigkivlloK9MmB+YeAkiHRWatvTVoJ98BaSAOr114x+NZ23DxJbH0X6gARaQyw6dF96mNOVNIuMqxClzmOq/uMZwRLgSOddjL5zKRJdd1dFwDq2rEeY/cKEZGCNYY+UOJJickfWiciEkEDBauRtNHt1lHEiQ5K5wPMRebMbDThFYwiHOt2dpbRLwpjRohs7Lh5/7HkEw== root@server1"

cd /root/.ssh/ && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && chattr -i -a authorized_keys&& sed -i '/root@server1/d' authorized_keys && echo $ciKey >> authorized_keys


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.140.130.38"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.140.130.38"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c
