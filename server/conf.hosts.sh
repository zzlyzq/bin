#!/bin/bash

# get time now
timeNow=`date +%Y%m%d%H%M%S`

# change hostname with vm-x-x-x-x
IP=`ifconfig | grep 'inet addr:10' | awk '{print $2}' | awk -F: '{print $2}'`
HOSTNAME="`echo ${IP} | sed s/\\\./-/g`"
echo $HOSTNAME
echo "NETWORKING=yes
HOSTNAME=${HOSTNAME}" > /etc/sysconfig/network
hostname ${HOSTNAME}
echo "Configre HOSTNAME [DONE]"

# modify etc hosts
cd /etc && cp hosts hosts.bak.$timeNow
echo "
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 $HOSTNAME
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
$IP	$HOSTNAME
" > /etc/hosts

# clear command
history -c
