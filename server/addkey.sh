#!/bin/bash

#set -x
#IFS=$'\n'
# add key for identify username

# get the username
name=$1;echo "Your target name is: $name"

# jenkins@10-180-86-29
keyname=$2

# 10.180.86.29
keyip=$3

# bigdata.vxlan.net::d/server/key.jenkins.10-180-86-29
keyfile=$4

echo $name
echo $keyname
echo $keyip
echo $keyfile
#exit 0


#homedir="123"

# get the workdir
#for line in `cat /etc/passwd`
#cat /etc/passwd | while read line
for line in $(cat /etc/passwd)
do
    #echo $line
    nameinfile=`echo $line | awk -F: '{print $1}'`
    homeinfile=`echo $line | awk -F: '{print $6}'`
    #echo -n $nameinfile " "
    #echo -n $homeinfile " "
    if [[ $name = $nameinfile ]];then
	homedir="$homeinfile"
	break
    fi
done
echo "homedir: $homedir"

# get the authorized_keys
if [[ -n $homedir ]];then
    if [[ $name = "root" ]];then
	targetDir="$homedir/.ssh/"
	targetFile="authorized_keys2"
    else
	targetDir="$homedir/.ssh/"
	targetFile="authorized_keys"
   fi
fi
echo "TargetDir: $targetDir"
echo "TargetFile: $targetFile"

if [ -n $targetDir -a -n $targetFile ];then
#if [ -n $targetDir -a -n $abc ];then
    echo "都存在"
fi


# insert key
timeNow=`date +%Y%m%d%H%M%S`
# 准备目录
#if [ -d $targetDir ];then
#    echo "targetDir exist."
#else
    mkdir $targetDir
    chown $name $targetDir
    chmod 700 $targetDir
    touch $targetDir/$targetFile
    chown $name $targetDir/$targetFile
    chmod 600 $targetDir/$targetFile
#fi

rsync -avzP $keyfile /tmp/pubkey.txt
cd $targetDir && touch $targetFile &&  cp $targetFile $targetFile.bak${timeNow} && sed -i "/$keyname/d" $targetFile && cat /tmp/pubkey.txt >> $targetFile

# 确保有services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# insert src ip
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetAllowFile="services_hosts_allow"
    targetAllowLine="sshd:$keyip"
    cd /etc && cp $targetAllowFile $targetAllowFile.bak${timeNow} && touch $targetAllowFile && chattr -i -a $targetAllowFile ; sed -i "/$targetAllowLine/d" $targetAllowFile; echo $targetAllowLine >> $targetAllowFile
else
    targetAllowFile="hosts.allow"
    targetAllowLine="sshd:$keyip"
    cd /etc && cp $targetAllowFile $targetAllowFile.bak${timeNow} && touch $targetAllowFile && chattr -i -a $targetAllowFile && sed -i "/$targetAllowLine/d" $targetAllowFile; echo $targetAllowLine >> $targetAllowFile
fi
