#!/bin/bash

javaVersion="jdk1.7.0_45"

cd /letv && rsync -avzP 10.180.92.193::d/jenkins/slave/soft/$javaVersion .
ln -s /letv/$javaVersion /usr/local/java
export JAVA_HOME=/usr/local/java
export PATH=$JAVA_HOME/bin:$PATH
echo "export JAVA_HOME=/usr/local/java
export PATH=$JAVA_HOME/bin:$PATH" >> /root/.bashrc
ln -s /usr/local/java/bin/java /usr/bin/java
