#!/bin/bash

ip="103.7.6.177"
ipname="103-7-6-177"

timeNow=`date +%Y%m%d%H%M%S`
key="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAv30Js8Hv46FpPf73nLZparA0eg4g7dMm4O0bcQe7Xl7Fu1Oqb4ftCtWXKza+Ibgw3DC5d9HV//mpEPqvqpFgPPJ0a91GoRRU0AZtiL8XNGb4uYO0jOvfW/yud411CQAarYmoiut+gXVBG7utXuglBhEomho2tNpPsPiaym5q4Ai//SaQAOKYzmNHOLNwl/ZahcM73Ux6VW3eC8H1BV6ut2IDp7jtRuDXdOws9xNnBD1uDV+uEonLxz1uQqIDEJ0WYv8ubuATQ9EDKWqq+foaSHYbur2UDv5uTFAG9z8IS9siaR96eRK4nVI7iAbs02rO9IwIsJ6juLTwveGKPiBkrw== jenkins@103-7-6-177"

cd /root/.ssh/  && echo "Enter Dir OK" || echo "Enter Dir Error"
 touch authorized_keys  && echo "touch key OK" || echo "touch key error"
 cp authorized_keys authorized_keys.bak${timeNow}  && echo "backup ok" || echo "backup error"
 chattr -i -a authorized_keys  && echo "clear att ok" || echo "clear att error"
 sed -i "/$ipname/d" authorized_keys  && echo "replace ok" || echo "replace error"
 echo $key >> authorized_keys && echo "Put key OK" && echo "insert key ok" || echo "insert key error"


Host="sshd:$ip"
cd /etc && cp hosts.allow hosts.allow.bak${timeNow} && touch hosts.allow && chattr -i -a hosts.allow && sed -i "/sshd:$ip/d" hosts.allow && echo $Host >> hosts.allow

history -c
