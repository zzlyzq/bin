#!/bin/bash

lanip=`ifconfig | grep 'inet addr:10' | awk '{print $2}' | awk -F: '{print $2}'`
defaultNetcard=`netstat -rn | grep ^0.0.0.0 | awk '{print $8}'`
curl "http://lc.vxlan.net/lineRealTime/get/get.defaultNetcard.php?lanip=$lanip&defaultNetcard=$defaultNetcard"
