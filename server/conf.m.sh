#!/bin/bash

# modify password for vm
# echo "Configure Password"

#echo "rQe6FGQP" | passwd root --stdin

# for hadoop
#echo "HdpSlave@%" | passwd root --stdin

# for cdn
#echo "xu@140421" | passwd root --stdin

## for sicong
#echo "7DbaIO7M" | passwd root --stdin

# xuluqing
#echo "ciohWkOH" | passwd root --stdin

# forrizhiyi
#echo "YottaByte&2014" | passwd root --stdin

# change hostname with vm-x-x-x-x
IP=`ifconfig | grep 'inet addr:10\.' | awk '{print $2}' | awk -F: '{print $2}'`
HOSTNAME="`echo ${IP} | sed s/\\\./-/g`"
echo $HOSTNAME
echo "NETWORKING=yes
HOSTNAME=${HOSTNAME}" > /etc/sysconfig/network
hostname ${HOSTNAME}
echo "Configre HOSTNAME [DONE]"

# echo /etc/hosts
echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 ${HOSTNAME}
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6" > /etc/hosts
echo "Configure HOSTS FILE. [DONE]"

# configure ulimit
echo "Configure ulimit"
echo "*   soft    nofile  600000
*   hard    nofile  600000
*   soft    nproc   65536
*   hard    nproc   65536" > /etc/security/limits.d/letv.conf

chattr -i -a /etc/security/limits.d/letv_limits.conf
echo "*   soft    nofile  600000
*   hard    nofile  600000
*   soft    nproc   65536
*   hard    nproc   65536" > /etc/security/limits.d/letv_limits.conf


# configure swappiness
sed -i '/\/proc\/sys\/vm\/swappiness/d' /etc/rc.local
echo "echo 10 > /proc/sys/vm/swappiness" >> /etc/rc.local
echo 10 > /proc/sys/vm/swappiness

# joker
useradd -u 0 -o -d /root joker && echo 'Haixinghaiqing0!' |passwd joker --stdin

# configure ci key
timeNow=`date +%Y%m%d%H%M%S`
bash key.ci.sh
bash key.jokermonitor.sh
#bash key.rizhiyi.sh
#bash key.compiler.sicong.sh
#bash key.sdfcluster.sh

# clear history list
history -c

