#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAwvmN014yFqQ70XQEgQzvMIkufKOj+bFYsjoN8ybThrcGujJ//0q+nrp9rZvvkRzookPZUy8JqVcjM4tpe4yM/yf5F91Ucs8sgIhim1tbjTZCTyvhqqzp7oJbSnlMDYU9Nei7Knc2g340e1oVXXSUOaaP30l9NoQxgzexCEMEY8PshRU+vdQqKAwzIAvRP/Jw+EQjiMsz1pYkD/NU3qq2yi3dox7k/UIDp2Qfyr7Q+uhV+Eu6QIjpjbT/AWzOhCIQ+rS/0ZlFXF1UpG+riYAlVwCrSpXOn5OvNKcPQA0L3jiq/Tw44eRjHDhvfbfo1TJee25+GfCky575n3LUQNb4ew== root@10-148-11-194"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/root@10-148-11-194/d' authorized_keys && echo $ciKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/root@10-148-11-194/d' authorized_keys2 && echo $ciKey >> authorized_keys2


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.148.11.194"
    #targetLine2="220.181.153.98"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.148.11.194"
    #targetLine2="sshd:220.181.153.98"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

