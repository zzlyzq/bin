#!/bin/bash
# For Hadoop Machine Init


timeNow=`date +%Y%m%d%H%M%S`
logserverKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzmorUqwHOvlFNnSPRsexcV3ahIIxGZ579cvzGBZq2CKRrDfvizMk7FhJZhEznL6owy7I2yTdhXUopJwvCtjvlqIV4ADX3/o4LRZdBhK94RP/nT49WJdPvRLhFl1vS0HN4vFE/f0761sC7cIyoDg47fnNZH4qhzPIFlQBr8nLLuYcvEI7NWqKOULXkzbFF+r9qcTA3xhlJNfZc4ASHEPCTER6R/VCW9EwlnzOoxziyQvdROAPdB2JrZeJ4oJ+fXz3Bb+4cFbwHmvoT73X1sdqi7i9yHhb6YIVUcG3PUbKvJ7V8JsyDsRYj6prvKOYeQYwDSMpyhxg1YzJZw4MqyR1VQ== root@sdf-logserver"

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/root@sdf-logserver/d' authorized_keys2 && echo $logserverKey >> authorized_keys2

cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.149.14.7"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.149.14.7"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c
