#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAv20KHH+WvemmNcOlx9z6uaJ+LY85ho5D2DXXkmNzBLb9jxefOYBR31Ly/5+HsDxGMERQO7+VjKcskPtTdDj19VuPprdOpNGrfGB+GJy0fJO9isBmDifzyp9/LuNE4l8/Hm4LDJxB6nqJ2au5dlqm9LhEW23dthUqIHoV1aXPXtULhazUC+rcsjzxGyAlnXfjSk/duhUBYXFo8C4LFs7jYFCy+3RgwR4xswC4H1lB2J50VNeHjY5xDJVz7yNVuqe9fPR9bouQ9/5Y4oO+E0yR/xmj2w/Hvj6T7KmeQtQXF/aCD01n5CEgbUpdZS9OiyS55Yc2omlG1O+GUdjJykwnhQ== yishenggudou@gmail.com"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/yishenggudou@gmail.com/d' authorized_keys && echo $ciKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/yishenggudou@gmail.com/d' authorized_keys2 && echo $ciKey >> authorized_keys2


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.150.140.68"
    #targetLine2="220.181.153.98"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.150.140.68"
    #targetLine2="sshd:220.181.153.98"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

