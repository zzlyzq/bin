#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuqR6QZYmVLooHbGS+Rdq6pnMD2NVgtO3GFxbbxpD/w1DcZ75cSA7r06CLh+7/83slQVCL8145bs/dgr3Fi2W7UDW7kYVl9NsJs85RTkDqFm8Nj4VlL03pWMrieGQe8wbzycT1lt+oStIGY/IZPg8A2t2uh4724xpna/gESEcdTtJH5sIqAyD1UEtUm0N4ppZaD6rK1tb4ywCB4Q19Mm/O95MQNm8bzIhLXiJJi5/PzI+rqrcfjracOSh1ioUXc2TVP17F/yGNk854LHbjvnwXjnGLxqSif+16ak62nlUJSNF5enuKPP5vGmyXNuo39xk6pwarO0fjn0kbck+3wZk7Q== root@hadoopNN1.com"
cd /root/.ssh/ && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && chattr -i -a authorized_keys&& sed -i '/root@hadoopNN1.com/d' authorized_keys && echo $ciKey >> authorized_keys

cd /etc
#touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.100.91.36"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.100.91.36"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c
