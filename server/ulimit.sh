echo "Configure ulimit"
echo "*   soft    nofile  600000
*   hard    nofile  600000
*   soft    nproc   65536
*   hard    nproc   65536" > /etc/security/limits.d/letv.conf
