#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAqjgqF6wjcDeSiFyTgJL6gWB4/lRykDceHL0kPcE2SUVj2dPXK+Q7bzmyRc0n5V7ZvBBfGeN8YdyV2pCz0uE607n6uY5yut5rQlsP+FZAL2de0jpZLCgn2Z0mS4IS0IbayWiXIE7KNsp5KVgYeI3630kZ9u+KKc+VDumYmG9AnbbC6CGfvD1FE5iXkm7F78So7GZTgFXEsPTGeXybDGK3BH+QXdPApvhrLhReVbGnFDa8WkFoIt30eX7AibP+05FWRFMCk4lODW9eihYVkzVGg9liT7nPfVtI1MjAf5Whblgmj8b0jP+YV8zxMtsSEKW5WbvyjL+bBrcf2NSJfQecPw== root@vm-10-142-164-56"

cd /root/.ssh/ && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && chattr -i -a authorized_keys&& sed -i '/root@vm-10-142-164-56/d' authorized_keys && echo $ciKey >> authorized_keys


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.142.164.56"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.142.164.56"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c
