#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
key="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAtGZ4NtllaSRWf9edS2Vzp4R63gCVw++fDf2GtQoRB1kutr17SojVcYpRVnigrkkwAbrg7fsdo2DPQ83DvYMIBaRbBWnb73sVUSuksUmdX9zGTPEiVcHH4ADg6w/p39BJ3gD4Npe1zPC6vvVWhymEAt4P5pe8rU6AulDAFCJFGrY07+LOm7JxXc1ArjfMjOjlCAyKBrjmTjr1HKp6Xj5KK+wMmgp/ftPuAc4s8pvSYPOwCZfvWQBwzz42/G+aIAE/UAWyEQS72yzBi7g81+yCW8J9g7KdPinh3tbYFQMJYb0a8vI1A3ikJ3r9KqCw+yuB+m9lZVH52usQo4Q0c9suyw== root@10-140-60-85"
cd /root/.ssh/  && echo "Enter Dir OK" || echo "Enter Dir Error"
 touch authorized_keys  && echo "touch key OK" || echo "touch key error"
 cp authorized_keys authorized_keys.bak${timeNow}  && echo "backup ok" || echo "backup error"
 chattr -i -a authorized_keys  && echo "clear att ok" || echo "clear att error"
 sed -i '/root@10-140-60-85/d' authorized_keys  && echo "replace ok" || echo "replace error"
 echo $key >> authorized_keys && echo "Put key OK" && echo "insert key ok" || echo "insert key error"


Host="sshd:10.140.60.85"
cd /etc && cp hosts.allow hosts.allow.bak${timeNow} && touch hosts.allow && chattr -i -a hosts.allow && sed -i '/sshd:10.140.60.85/d' hosts.allow && echo $Host >> hosts.allow

history -c
