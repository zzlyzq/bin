#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
key="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAufTxTIu5j8o/oW0NDvZP74KxyenfdUffEacHghP19vsWxpeeHmYdNnXFzrQYR8v8LdMSpibjAKc/3So28qmfs1bQsuyrI4i5uTR2pWIWTnMABNRpGHBqyNLSLG45zp3u31d4lm6bWdZjGecb5D2+gfb+AF2cmJb5n/RsvwZ+WXLNXM5pZr7sqJ3QlgmyWOrGPtTo8+KwDSeJcBnt3geE4vfH42skCcErAfnTdXMzt0nv3tEcmxjYqdnwJ/mXSYJsY8/cjShft2zslexVeuVYSCzHeZfp+fgdJxxx7FFe2/YfgGPrGSVNC5ceI5LzBhpEsy2d2cd9TWXt1cvN97/Yow== root@sdf-nimbus-cdn"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/root@sdf-nimbus-cdn/d' authorized_keys && echo $key >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/root@sdf-nimbus-cdn/d' authorized_keys2 && echo $key >> authorized_keys2

# 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.149.11.66"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.149.11.66"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c

