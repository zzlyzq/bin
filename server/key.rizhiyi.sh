#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAx0luswa0xx+gg00PIVxAw7i9Ww/wgG2Zv9jtUwhlhWb4enBoIpbqBeorClGpHiF4EOpC5pd2daF0v7xyCJZ4xYenaa1xcjT6UPh8CBrGFh+YvhervanZG+iZhXbH23gAJQnU2KQqe+nEJN3UVpGAtN/fCXxJ7e3mQ4BqxA9xpCVh8ilsl8qmMqMvNbFRVvZIa8YYo9Ag8+r7zrVdsSjzMcGkVi918PgQtMdIi+tZ0ApFnvSlwQbdZX7KcToxK3BznHbS+zxQTWHDw4kNh2D9fcimO/GWQJdyvTlAQ3xAgoGHr6Uf0bXYfeJ6R0nL+I7rJYQczRiCq3f3YjOwIu4f5Q== root@10-180-92-205"

cd /root/.ssh/ && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && chattr -i -a authorized_keys&& sed -i '/root@10-180-92-205/d' authorized_keys && echo $ciKey >> authorized_keys


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.180.92.205"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.180.92.205"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c
