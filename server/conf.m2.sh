#!/bin/bash

# modify password for m2
echo "Configure Password"

echo "5ovRo9pk" | passwd root --stdin

# change hostname with vm-x-x-x-x
IP=`ifconfig | grep 'inet addr:10' | awk '{print $2}' | awk -F: '{print $2}'`
HOSTNAME="`echo ${IP} | sed s/\\\./-/g`"
echo $HOSTNAME
echo "NETWORKING=yes
HOSTNAME=${HOSTNAME}" > /etc/sysconfig/network
hostname ${HOSTNAME}
echo "Configre HOSTNAME [DONE]"

# echo /etc/hosts
echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 ${HOSTNAME}
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6" > /etc/hosts
echo "Configure HOSTS FILE. [DONE]"

# configure ulimit
echo "Configure ulimit"
echo "*   soft    nofile  600000
*   hard    nofile  600000
*   soft    nproc   65536
*   hard    nproc   65536" > /etc/security/limits.d/letv.conf

#chattr -i -a /etc/security/limits.d/letv_limits.conf
#echo "*   soft    nofile  600000
#*   hard    nofile  600000
#*   soft    nproc   65536
#*   hard    nproc   65536" > /etc/security/limits.d/letv_limits.conf

# configure swappiness
sed -i '/\/proc\/sys\/vm\/swappiness/d' /etc/rc.local
echo "echo 10 > /proc/sys/vm/swappiness" >> /etc/rc.local
echo 10 > /proc/sys/vm/swappiness

# clear hadoop key
cd /root/.ssh/
sed -i '/root@10-140-60-85/d' authorized_keys 
sed -i '/root@sdf-namenode2/d' authorized_keys
sed -i '/root@sdf-resourcemanager2/d' authorized_keys
sed -i '/root@sdf-namenode1/d' authorized_keys
sed -i '/root@sdf-resourcemanager1/d' authorized_keys
sed -i '/root@sdf-spark-master1/d' authorized_keys
sed -i '/root@sdf-spark-master2/d' authorized_keys
sed -i '/root@sdf-logserver/d' authorized_keys

cd /etc/
sed -i '/sshd:10.140.60.85/d' hosts.allow


# clear history list
history -c

