#!/bin/bash
cd /tmp
[ $? -eq 0 ] && umount /data/slot{0..9}
[ $? -eq 0 ] && umount /data/slota
[ $? -eq 0 ] && umount /data/slotb
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdb
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdc
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdd
[ $? -eq 0 ] && mkfs.xfs -f /dev/sde
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdf
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdg
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdh
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdi
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdj
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdg
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdk
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdl
[ $? -eq 0 ] && mkfs.xfs -f /dev/sdm
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdb        /data/slot0
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdc        /data/slot1
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdd        /data/slot2
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sde        /data/slot3
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdf        /data/slot4
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdg        /data/slot5
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdh        /data/slot6
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdi        /data/slot7
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdj        /data/slot8
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdk        /data/slot9
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdl        /data/slota
[ $? -eq 0 ] && /bin/mount -t xfs -o defaults,noatime,nodiratime        /dev/sdm        /data/slotb
