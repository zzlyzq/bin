#!/bin/bash

netcard=`netstat -rn | grep ^0.0.0.0 |  awk '{print $8}'`
echo $netcard
IP=`ifconfig | grep 'inet addr:10' | awk '{print $2}' | awk -F: '{print $2}'`
bridgeid=`tcpdump -i $netcard stp -n -vvv -c 1 2>/dev/null | grep bridge-id | awk '{print $10}' | awk -F, '{print $1}'`
[[ $bridgeid -eq "length" ]] && bridgeid=`tcpdump -i $netcard stp -n -vvv -c 1 2>/dev/null | grep bridge-id | awk '{print $9}' | awk -F, '{print $1}'`
# if Forward]
[[ ${#bridgeid} -eq 8 ]] && bridgeid=`tcpdump -i $netcard stp -n -vvv -c 1 2>/dev/null | grep bridge-id | awk '{print $11}' | awk -F, '{print $1}'`
echo $bridgeid
swpriority=`echo $bridgeid | awk -F. '{print $1}'`
swname=`echo $bridgeid | awk -F. '{print $2}'`
swport=`echo $bridgeid | awk -F. '{print $3}'`
echo $swpriority
echo $swname
echo $swport
curl "http://lc.vxlan.net/lineRealTime/get/update.sw.php?lanip=$IP&swname=$swname&swport=$swport"
