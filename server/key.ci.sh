#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAojWIx3+56g0LAIDPA9LFg0+ypDaauckwJHTxbZsiMMU8tXeD6QJuvcFeswXbH4ahzWDD1B5frftpvBAktEykX/33287UEQG5lLCoFbPqf4TNz1F+wUv1WEjcgp1WlIjK0U8tK51x27kXVHKQk38FsZWx44DJkr06Eoik4YrxdKWmU8FMj9vbHemtilp18S2L7gweTovMGkpMBBfd2rGCFhY22cXC3TDJrszH8nPoWGr+y/1lauiL7UYn9EsnQGAWLueM/Zkqw7405TuKkfxjwddFLEfwjEXrgrG52GueUTmODUTqAmuiHEXOTWBQA9xBt9Namv+Glx5yhPj7SgE0rQ== jenkins@10-180-86-29"

cd /root/.ssh/ && chattr -i -a authorized_keys && touch authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && sed -i '/jenkins@10-180-86-29/d' authorized_keys && echo $ciKey >> authorized_keys

cd /root/.ssh/ && touch authorized_keys2 &&  cp authorized_keys2 authorized_keys2.bak${timeNow} && sed -i '/jenkins@10-180-86-29/d' authorized_keys2 && echo $ciKey >> authorized_keys2


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.180.86.29"
    targetLine2="220.181.153.98"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.180.86.29"
    targetLine2="sshd:220.181.153.98"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile && echo $targetLine2 >> $targetFile
fi

history -c

