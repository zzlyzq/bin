#!/bin/bash
# For Hadoop Machine Init

timeNow=`date +%Y%m%d%H%M%S`
ciKey="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuXQ9CY8u3SK2RhoCLiIxVk3oseYWap2P78BJmL2KhzRJwVpKsuQhc9CTBPFmhReMlL+n/nC8uZeEIoaROi+4Oipexx1gZSn8g3qit4i1v7lfrGD/O7qaMWZGbccoANsszfvOBsconufSWVkWYV3fNfewlqVLR/O646fSFtVDJZTU0Gm2pxOSvSPFJyZ+82hFZRP/Xv+fg4R9JAlbk6dZWqOeeb21ZVGx1gL5Xdo1pdG4nfDFW2yvu/AxHFi6J+A/8OltbS2s8FxWcMJ5lKsimmo3l9thVuw4h4ypkEQqGunFGrxTNwwtP553KMArYi/S76s0m8wbg3Lj9e1fIPZbgw== hadoop@server1"

cd /home/hadoop/ && [ -d "/home/hadoop/.ssh" ] || mkdir .ssh
cd /home/hadoop/ && [ -d "/home/hadoop/.ssh" ] && chown hadoop:hadoop .ssh || exit
cd /home/hadoop/ && [ -d "/home/hadoop/.ssh" ] && chmod 700 .ssh || exit

cd /home/hadoop/.ssh/ && touch authorized_keys && chown hadoop:hadoop authorized_keys && chmod 400 authorized_keys && cp authorized_keys authorized_keys.bak${timeNow} && chattr -i -a authorized_keys&& sed -i '/hadoop@server1/d' authorized_keys && echo $ciKey >> authorized_keys


# 2015-06-24 确保/etc/hosts.allow 里面有 sshd:/etc/services_hosts_allow
cd /etc && touch hosts.allow && cp hosts.allow hosts.allow.bak${timeNow} && chattr -i -a hosts.allow && sed -i '/services_hosts_allow/d' hosts.allow && echo 'sshd:/etc/services_hosts_allow' >> hosts.allow

# 部署key
cd /etc
touch services_hosts_allow
if [ -f "services_hosts_allow" ]
then
    echo "发现目标文件"
    targetFile="services_hosts_allow"
    targetLine="10.140.130.38"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile ; sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
else
    targetFile="hosts.allow"
    targetLine="sshd:10.140.130.38"
    cd /etc && cp $targetFile $targetFile.bak${timeNow} && touch $targetFile && chattr -i -a $targetFile && sed -i "/$targetLine/d" $targetFile; echo $targetLine >> $targetFile
fi

history -c
