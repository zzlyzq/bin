service snmpd stop
chkconfig snmpd off
rsync -avzP snmpd2.init /etc/init.d/snmpd2
rsync -avzP /usr/sbin/snmpd /usr/sbin/snmpd2
chkconfig snmpd2 on
service snmpd2 restart
