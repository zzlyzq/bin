tar xzvf nrpe.2.15.tar.gz
cd nrpe-2.15
./configure --enable-command-args
make
make install
service nrpe restart
/usr/local/nagios/libexec/check_nrpe -H 127.0.0.1 -c cacti_sockstat -a socketsUsed
