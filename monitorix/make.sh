#!/bin/bash

[ -e "/etc/monitorix/monitorix.conf" ] && exit 0

#export http_proxy=http://10.180.86.30:3128/
#cd /etc/yum.repos.d && mkdir bak ; mv * bak/
#cd /etc/yum.repos.d && rsync -avzP bigdata.vxlan.net::d/repo/one.repo .
#yum clean all && yum makecache
#yum install rrdtool rrdtool-perl perl-libwww-perl perl-MailTools perl-MIME-Lite perl-CGI perl-DBI perl-XML-Simple perl-Config-General perl-HTTP-Server-Simple perl-IO-Socket-SSL -y

#export http_proxy=http://10.180.86.30:3128/
#cd /bin/ && rsync -avzP bigdata.vxlan.net::d/monitorix/cpanm . && chmod a+x cpanm
#rsync -avzP cpanm /bin/
#cpanm HTTP::Server::Simple

# config
#cd /tmp/ && rsync -avzP bigdata.vxlan.net::d/monitorix/monitorix-3.7.0-1.noarch.rpm .
#rpm -Uvh monitorix-3.6.0-1.noarch.rpm
#cd /etc/monitorix/ && rsync -avzP bigdata.vxlan.net::d/monitorix/monitorix.conf monitorix.conf
yum install -y monitorix
rsync -avzP monitorix.conf /etc/monitorix/
service monitorix restart
chkconfig monitorix on
