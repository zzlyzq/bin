#!/bin/bash

mkdir -p /data/rra/cacti/
rsync -avzP cacti.tar.gz /data/rra/cacti/
cd /data/rra/cacti && tar xzvf cacti.tar.gz
