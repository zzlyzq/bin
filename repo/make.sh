#!/bin/bash
cd /etc/yum.repos.d/ && mkdir bak ; mv * bak/
cd -

rsync -avzP one.repo /etc/yum.repos.d/

yum clean all
yum makecache
